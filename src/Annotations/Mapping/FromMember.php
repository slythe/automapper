<?php
namespace Sapientes\Automapper\Annotations\Mapping;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 *
 * @AnnotationStrategy
 * @Target("PROPERTY")
 */
class FromMember {
    /** @var  string */
	public $source;
}