<?php
namespace Sapientes\Automapper\Annotations\Mapping;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 *
 * @AnnotationStrategy
 * @Target("PROPERTY")
 */
class Association {
    /** @var  string */
    public $destination;
    /** @var  string */
    public $source;
}