<?php
namespace Sapientes\Automapper\Annotations\Mapping;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 *
 * @AnnotationStrategy
 * @Target("PROPERTY")
 */
class FromFunction {
    /** @var  string */
    public $function;
    /** @var  string */
    public $source;
    /** @var  array */
    public $params = [];
}