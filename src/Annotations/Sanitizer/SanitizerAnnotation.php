<?php
namespace Sapientes\Automapper\Annotations\Sanitizer;

use Illuminate\Support\Collection;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
interface SanitizerAnnotation {
    /**
     * Get sanitizer object
     *
     * @return mixed
     */
    public function getSanitizerName() : string;
    
    /**
     * Get annotation as collection.
     *
     * @return Collection
     */
    public function toCollection() : Collection;
}