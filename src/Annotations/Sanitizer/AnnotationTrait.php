<?php
namespace Sapientes\Automapper\Annotations\Sanitizer;

use Illuminate\Support\Collection;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
trait AnnotationTrait {
    
    /**
     * Get annotation as collection.
     *
     * @return Collection
     */
    public function toCollection() : Collection {
        return Collection::make(get_object_vars($this));
    }
}