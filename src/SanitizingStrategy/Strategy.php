<?php
namespace Sapientes\Automapper\SanitizingStrategy;


/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
interface Strategy {
    /**
     * Sanitize value for given property.
     *
     * @param                     $value
     * @param \ReflectionProperty $property
     *
     * @return mixed Sanitized value
     */
    public function sanitize($value, \ReflectionProperty $property);
}