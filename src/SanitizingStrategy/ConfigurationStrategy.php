<?php
namespace Sapientes\Automapper\SanitizingStrategy;

use Illuminate\Support\Collection;
use Sapientes\Automapper\Sanitizers\SanitizerRegistry;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class ConfigurationStrategy implements Strategy {
    /** @var  Collection */
    protected $config;
    
    /**
     * ConfigurationStrategy constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = []) {
        $this->config = collect($config);
    }
    
    /**
     * @inheritdoc
     */
    public function sanitize($value, \ReflectionProperty $property) {
        $source = new Source($value);
        
        if($this->config->has($class = $property->getDeclaringClass()->getName())) {
            $classConfig = collect($this->config->get($class));
            if($classConfig->has($property->getName())) {
                foreach ((array) $classConfig->get($property->getName()) as $sanitizerConfig) {
                    $sanitizerConfig = collect($sanitizerConfig);
                    if(! $sanitizerConfig->has('name')) throw new \RuntimeException("No sanitizer name in config for property '{$property->getName()}'.");
                    
                    if(! SanitizerRegistry::has($name = $sanitizerConfig->get('name'))) throw new \RuntimeException("Sanitizer '{$name}' is not registered in registry.");
    
                    SanitizerRegistry::get($name)->sanitize($source, collect($sanitizerConfig->get('options')));
                }
            }
        }
        
        return $source->getSource();
    }
}