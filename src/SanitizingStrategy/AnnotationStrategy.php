<?php
namespace Sapientes\Automapper\SanitizingStrategy;

use Doctrine\Common\Annotations\Reader;
use Sapientes\Automapper\Annotations\Sanitizer\SanitizerAnnotation;
use Sapientes\Automapper\Sanitizers\SanitizerRegistry;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class AnnotationStrategy implements Strategy {
    
    /** @var  Reader */
    protected $reader;
    
    /**
     * AnnotationStrategy constructor.
     *
     * @param Reader $reader
     */
    public function __construct(Reader $reader) {
        $this->reader = $reader;
    }
    
    /**
     * @inheritdoc
     */
    public function sanitize($value, \ReflectionProperty $property) {
        $annotations = array_filter($this->reader->getPropertyAnnotations($property), function($annotation) {
            return $annotation instanceof SanitizerAnnotation;
        });
    
        $source = new Source($value);
        
        /** @var SanitizerAnnotation $annotation */
        foreach ($annotations as $annotation) {
            if(! SanitizerRegistry::has($annotation->getSanitizerName())) throw new \InvalidArgumentException("Sanitizer '{$annotation->getSanitizerName()}' is not registered in registry.");
            
            SanitizerRegistry::get($annotation->getSanitizerName())->sanitize($source, $annotation->toCollection());
        }
        
        return $source->getSource();
    }
}