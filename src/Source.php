<?php
namespace Sapientes\Automapper;

use Illuminate\Support\Arr;
use Sapientes\Automapper\SourceDataPicker\ArraySourcePicker;
use Sapientes\Automapper\SourceDataPicker\ObjectSourcePicker;
use Sapientes\Automapper\SourceDataPicker\SourcePicker;
use TRex\Reflection\CallableReflection;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class Source {
    /** @var  mixed */
    protected $source;
    /** @var SourcePicker[]  */
    protected $supportedPickers = [];
    
    /**
     * Source constructor.
     *
     * @param $source
     */
    public function __construct($source) {
        $this->source = $source;
        
        $this->setSupportedSourcePickers([
            new ArraySourcePicker(),
            new ObjectSourcePicker(),
         ]);
    }
    
    /**
     * Pick value from source.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function pickValue(string $name) {
        $source = $this->source;
        foreach ($this->explodePath($name) as $segment) {
            $source = $this->getValue($source, $segment);
        }
        
        return $source;
    }
    
    /**
     * Transform value of source.
     *
     * @param string   $name
     * @param callable $transform
     */
    public function transformValue(string $name, callable $transform) {
        $parts = [&$this->source];
        foreach ($this->explodePath($name) as $segment) {
            $last = &$this->lastReference($parts);
            $parts[] = &$this->getValue($last, $segment);
        }
        
        $last = &$this->lastReference($parts);
        $last = $transform($last);
    }
    
    /**
     * Pick function from source.
     *
     * @param string $path
     * @param string $name
     *
     * @return CallableReflection
     */
    public function pickFunction(string $path, string $name) : CallableReflection {
        $source = $this->source;
        foreach ($this->explodePath($path) as $segment) {
            $source = $this->getValue($source, $segment);
        }
        
        return $this->getFunction($source, $name);
    }
    
    /**
     * Check if source is scalar.
     *
     * @return bool
     */
    public function isScalar() : bool {
        return is_scalar($this->source);
    }
    
    /**
     * Get whole source.
     *
     * @return mixed
     */
    public function getSource() {
        return $this->source;
    }
    
    /**
     * Set source
     *
     * @param $source
     */
    public function setSource($source) {
        $this->source = $source;
    }
    
    /**
     * Get last reference of reference array
     * @param $array
     *
     * @return mixed
     */
    protected function &lastReference(array &$array) {
        end($array);
        return $array[key($array)];
    }
    
    /**
     * Explode path into segments.
     *
     * @param string $path
     *
     * @return array
     */
    protected function explodePath(string $path) {
        return array_filter(explode('.', $path), function($value) {
            return ! empty($value);
        });
    }
    
    /**
     * @param  mixed $source
     * @param string $name
     *
     * @return \TRex\Reflection\CallableReflection
     */
    protected function getFunction($source, string $name) {
        if(empty($name)) return $source;
    
        $type = gettype($source);
        if(! array_key_exists($type, $this->supportedPickers)) throw new \RuntimeException("Cannot handle source type {$type}");
    
        return $this->supportedPickers[$type]->pickFunction($source, $name);
    }
    
    /**
     * Set supported source pickers.
     *
     * @param SourcePicker[] $pickers
     */
    protected function setSupportedSourcePickers(array $pickers) {
        $this->supportedPickers = [];
        
        foreach ($pickers as $key => $picker) {
            if(! $picker instanceof SourcePicker) throw new \InvalidArgumentException(sprintf("Value on key '%s' is not instance of %s", $key, SourcePicker::class));
            
            $this->supportedPickers[$picker->getSourceTypeName()] = $picker;
        }
    }
    
    /**
     * Get value from source.
     *
     * @param mixed  $source
     * @param string $name
     *
     * @return mixed
     */
    protected function &getValue(&$source, string $name) {
        if(empty($name)) return $source;
        
        $type = gettype($source);
        if(! array_key_exists($type, $this->supportedPickers)) throw new \RuntimeException("Cannot handle source type {$type}");
        
        return $this->supportedPickers[$type]->pickValue($source, $name);
    }
}