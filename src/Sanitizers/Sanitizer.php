<?php
namespace Sapientes\Automapper\Sanitizers;

use Illuminate\Support\Collection;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
interface Sanitizer {
    /**
     * Sanitize given value.
     *
     * @param Source     $source
     * @param Collection $options
     *
     * @return mixed
     * @internal param $value
     */
    public function sanitize(Source $source, Collection $options);
}