<?php
namespace Sapientes\Automapper\Sanitizers;

use Sapientes\Automapper\Sanitizers\String\Prepend;
use Sapientes\Automapper\Sanitizers\String\UpperCase;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class SanitizerRegistry {
    protected static $sanitizers = [];
    
    /**
     * Set sanitizer with given name into registry.
     *
     * @param string    $name
     * @param Sanitizer $sanitizer
     */
    public static function add(string $name, Sanitizer $sanitizer) {
        self::$sanitizers[$name] = $sanitizer;
    }
    
    /**
     * Fill registry with given values. Old values will be removed.
     *
     * @param array $sanitizers
     */
    public static function set(array $sanitizers) {
        self::clean();
        self::merge($sanitizers);
    }
    
    /**
     * Clean registry.
     */
    public static function clean() {
        self::$sanitizers = [];
    }
    
    /**
     * Merge current registry with given array.
     * Old sanitizers inside registry will be overwritten, if names equal.
     *
     * @param array $sanitizers [name => sanitizer, ...]
     */
    public static function merge(array $sanitizers) {
        foreach ($sanitizers as $name => $sanitizer) {
            self::add($name, $sanitizer);
        }
    }
    
    /**
     * Remove sanitizer by name.
     *
     * @param string $name
     */
    public static function remove(string $name) {
        unset(self::$sanitizers[$name]);
    }
    
    /**
     * Check if registry has sanitizer with name.
     *
     * @param string $name
     *
     * @return bool
     */
    public static function has(string $name) : bool {
        return array_key_exists($name, self::$sanitizers);
    }
    
    /**
     * Check if registry contains sanitizer.
     *
     * @param Sanitizer $sanitizer
     *
     * @return bool
     */
    public static function contains(Sanitizer $sanitizer) {
        return in_array($sanitizer, self::$sanitizers);
    }
    
    /**
     * Get sanitizer from registry.
     *
     * @param string $name
     *
     * @return Sanitizer
     */
    public static function get(string $name) : Sanitizer {
        return self::$sanitizers[$name];
    }
    
    /**
     * Init default sanitizers.
     */
    public static function initDefault() {
        self::set([
             UpperCase::class => new UpperCase(),
             Prepend::class => new Prepend(),
        ]);
    }
}