<?php
namespace Sapientes\Automapper\MappingStrategy;

use Doctrine\Common\Annotations\{
    AnnotationRegistry, Reader
};
use Sapientes\Automapper\Annotations\Mapping\NoMap;
use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\MappingStrategy\AnnotationStrategy\{
    AssociationsStrategy, AssociationStrategy, FunctionStrategy, MemberStrategy
};
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class AnnotationStrategy implements Strategy {
	
	/** @var  Reader */
	protected $reader;
	
	/** @var AnnotationStrategy\AnnotationStrategy[] */
	protected $annotationStrategies = [];
	
	/**
	 * AnnotationStrategy constructor.
	 *
	 * @param Reader $reader
	 */
	public function __construct(Reader $reader)  {
	    $this->reader = $reader;
	    $this->annotationStrategies = [
            new MemberStrategy($reader),
            new FunctionStrategy($reader),
            new AssociationStrategy($reader),
            new AssociationsStrategy($reader),
        ];
	    
	    $this->registerAnnotations();
	}
    
    /**
	 * @inheritdoc
	 */
	public function getValue(Source $source, \ReflectionProperty $property, Mapper $mapper) {
		foreach ($this->annotationStrategies as $strategy) {
		    if($strategy->canBeUsed($source, $property))
                return $strategy->getValue($source, $property, $mapper);
        }
        
        throw new \RuntimeException("Unable to get value for '{$property->getName()}' property.");
	}
    
    /**
     * @inheritdoc
     */
    public function shouldMap(Source $source, \ReflectionProperty $property): bool {
         return is_null($this->reader->getPropertyAnnotation($property, NoMap::class));
    }
    
    /**
     * Register strategy annotations
     */
    protected function registerAnnotations() {
        AnnotationRegistry::registerLoader('class_exists');
    }
}