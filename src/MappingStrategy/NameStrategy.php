<?php
namespace Sapientes\Automapper\MappingStrategy;

use Camel\Format\FormatInterface;
use Illuminate\Support\Str;
use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class NameStrategy implements Strategy {
    
    /** @var  FormatInterface */
    protected $format;
    /*** @var string */
    protected $noMapPrefix;
    
    /**
     * NameStrategy constructor.
     *
     * @param FormatInterface $format
     * @param string          $noMapPrefix
     */
    public function __construct(FormatInterface $format, $noMapPrefix = '_') {
        $this->format = $format;
        $this->noMapPrefix = $noMapPrefix;
    }
    
    /**
     * @inheritDoc
     */
    public function shouldMap(Source $source, \ReflectionProperty $property): bool {
        return ! Str::startsWith($property->getName(), $this->noMapPrefix);
    }
    
    /**
     * @inheritDoc
     */
    public function getValue(Source $source, \ReflectionProperty $property, Mapper $mapper) {
        $words = array_map(function($value){
            return lcfirst($value);
        }, $this->format->split($property->getName()));
        
        return $source->pickValue(implode('.', $words));
    }
}