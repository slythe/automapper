<?php
namespace Sapientes\Automapper\MappingStrategy;

use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
interface Strategy {
    
    /**
     * Get value from source for given property.
     *
     * @param Source              $source
     * @param \ReflectionProperty $property
     * @param Mapper              $mapper
     *
     * @return mixed
     */
    public function getValue(Source $source, \ReflectionProperty $property, Mapper $mapper);
    
    /**
     * Check, property should be mapped.
     *
     * @param Source              $source
     * @param \ReflectionProperty $property
     *
     * @return mixed
     */
    public function shouldMap(Source $source, \ReflectionProperty $property) : bool;
}