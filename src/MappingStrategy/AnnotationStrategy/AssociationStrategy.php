<?php
namespace Sapientes\Automapper\MappingStrategy\AnnotationStrategy;

use Doctrine\Common\Annotations\Reader;
use Sapientes\Automapper\Annotations\Mapping\Association;
use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class AssociationStrategy implements AnnotationStrategy {
    
    /** @var  Reader */
    protected $reader;
    
    /**
     * PropertyStrategy constructor.
     *
     * @param Reader $reader
     */
    public function __construct(Reader $reader) {
        $this->reader = $reader;
    }
    
    /**
     * @inheritdoc
     */
    public function canBeUsed(Source $source, \ReflectionProperty $property): bool {
        return $this->reader->getPropertyAnnotation($property, Association::class) !== null;
    }
    
    /**
     * @inheritdoc
     */
    public function getValue(Source $source, \ReflectionProperty $property, Mapper $mapper) {
        /** @var Association $annotation */
        $annotation = $this->reader->getPropertyAnnotation($property, Association::class);
        return $mapper->map($source->pickValue($annotation->source ?? '.') , $annotation->destination);
    }
}