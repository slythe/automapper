<?php
namespace Sapientes\Automapper\MappingStrategy\AnnotationStrategy;

use Doctrine\Common\Annotations\Reader;
use Sapientes\Automapper\Annotations\Mapping\Associations;
use Sapientes\Automapper\Mapper;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class AssociationsStrategy implements AnnotationStrategy {
    
    /** @var  Reader */
    protected $reader;
    
    /**
     * PropertyStrategy constructor.
     *
     * @param Reader $reader
     */
    public function __construct(Reader $reader) {
        $this->reader = $reader;
    }
    
    /**
     * @inheritdoc
     */
    public function canBeUsed(Source $source, \ReflectionProperty $property): bool {
        return $this->reader->getPropertyAnnotation($property, Associations::class) !== null;
    }
    
    /**
     * @inheritdoc
     */
    public function getValue(Source $source, \ReflectionProperty $property, Mapper $mapper) {
        /** @var Associations $annotation */
        $annotation = $this->reader->getPropertyAnnotation($property, Associations::class);
        $array = $source->pickValue($annotation->source ?? '.');
        
        if($array instanceof \Traversable) throw new \RuntimeException("Unable to create associations for property '{$property->getName()}'");
        
        return array_map(function($value) use ($mapper, $annotation){
            return $mapper->map($value , $annotation->destination);
        }, $array);
    }
}