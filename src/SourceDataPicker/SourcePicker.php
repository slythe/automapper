<?php
namespace Sapientes\Automapper\SourceDataPicker;

use TRex\Reflection\CallableReflection;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
interface SourcePicker {
    /**
     * Pick value from source.
     *
     * @param mixed  $source
     * @param string $name
     *
     * @return mixed
     */
    public function &pickValue(&$source, string $name);
    
    /**
     * Pick function from source
     *
     * @param mixed  $source
     * @param string $name
     *
     * @return mixed
     */
    public function pickFunction($source, string $name): CallableReflection;
    
    /**
     * Get source type name
     *
     * @return string
     */
    public function getSourceTypeName(): string;
}