<?php
namespace Sapientes\Automapper\SourceDataPicker;

use TRex\Reflection\CallableReflection;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class ObjectSourcePicker implements SourcePicker {
	
    /**
	 * @inheritdoc
	 */
	public function &pickValue(&$source, string $name) {
        if(! property_exists($source, $name)) {
            throw new \RuntimeException(sprintf("Property '%s' does not exists in '%s'", $name, get_class($source)));
        }
		
		return $source->$name;
	}
    
    /**
     * @inheritdoc
     */
    public function getSourceTypeName(): string {
        return 'object';
    }
    
    /**
     * @inheritdoc
     */
    public function pickFunction($source, string $name): CallableReflection {
        $object = new \ReflectionObject($source);
        
        return $object->hasMethod($name) ?
            new CallableReflection([$source, $name]) :
            new CallableReflection($this->pickValue($source, $name));
    }
}