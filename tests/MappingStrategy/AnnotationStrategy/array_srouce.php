<?php
/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */

return [
    'person' => [
        'name' => 'Person Name',
        'getAge' => function() {
            return 100;
        }
    ],
    'companies' => [
        ['name' => 'First Company'],
        ['name' => 'Second Company']
    ],
    'getText' => function() {
        return 'Just some text';
    },
    'getObject' => function() {
        return (object) ['property' => 'value'];
    },
];