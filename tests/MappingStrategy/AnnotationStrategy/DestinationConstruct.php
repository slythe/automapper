<?php
namespace Sapientes\Automapper\Tests\MappingStrategy\AnnotationStrategy;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
trait DestinationConstruct {
    public function __construct(array $values = []) {
        foreach (get_object_vars($this) as $name => $value) {
            if(array_key_exists($name, $values)) {
                $this->$name = $values[$name];
            }
        }
    }
}