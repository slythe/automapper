<?php
namespace Sapientes\Automapper\Tests\MappingStrategy\NameStrategy;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class PersonSource {
    public $name;
    
    /**
     * PersonSource constructor.
     *
     * @param $name
     */
    public function __construct($name) {
        $this->name = $name;
    }
}