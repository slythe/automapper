<?php
namespace Sapientes\Automapper\Tests\Sanitizers\String;

use Illuminate\Support\Str;
use Sapientes\Automapper\Sanitizers\String\LowerCase;
use Sapientes\Automapper\Source;

/**
 * @author Pavel Parma <pavelparma0@gmail.com>
 */
class LowerCaseTest extends \PHPUnit_Framework_TestCase {
    
    /** @var  LowerCase */
    protected $sanitizer;
    /** @var  string */
    protected $randomText;
    
    protected function setUp() {
        $this->sanitizer = new LowerCase();
        $this->randomText = str_random(16);
    }
    
    public function testScalar() {
        $source = new Source($this->randomText);
        $this->sanitizer->sanitize($source, collect());
        
        $this->assertEquals(Str::lower($this->randomText), $source->getSource());
    }
    
    public function testNestedSourceObject() {
        $property = 'nested';
        $source = new Source((object) [$property => $this->randomText]);
        $this->sanitizer->sanitize($source, collect(['source' => $property]));
        
        $this->assertEquals(Str::lower($this->randomText), $source->getSource()->$property);
    }
    
    public function testNestedSourceArray() {
        $key = 'nested';
        $source = new Source([$key => $this->randomText]);
        $this->sanitizer->sanitize($source, collect(['source' => $key]));
        
        $this->assertEquals(Str::lower($this->randomText), $source->getSource()[$key]);
    }
}
